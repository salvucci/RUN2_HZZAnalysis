#ifndef __doPlotsLinkDef_h__
#define __doPlotsLinkDef_h__

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link off all typedef;
#pragma link C++ nestedclass;
#pragma link C++ class doPlots+;

#endif

#endif
